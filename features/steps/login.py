# Would use Page Object Model to model web interactions

from behave import *

URL = 'https://www.hudl.com/login'


@given(u'"authorize" user login')
def step_impl(context):
    context.browser.get(URL)
    email_address = context.browser.find_element_by_id('email')
    email_address.send_keys('JEHalls81@gmail.com')
    password = context.browser.find_element_by_id('password')
    password.send_keys('Bismarck41')
    login_button = context.browser.find_element_by_id('logIn')
    login_button.click()


@then(u'should see username displayed on the screen')
def step_impl(context):
    user_profile = context.browser.find_element_by_id('explore-header')


@when(u'"unauthorized" user login')
def step_impl(context):
    context.browser.get(URL)
    email_address = context.browser.find_element_by_id('email')
    email_address.send_keys('JEHalls81@gmail.com')
    password = context.browser.find_element_by_id('password')
    password.send_keys('Bismrck41')
    login_button = context.browser.find_element_by_id('logIn')
    login_button.click()


@then(u'should see an error message displayed on the screen')
def step_impl(context):
    error_message = context.browser.find_element_by_class_name('login-error-container')
    assert error_message == "We didn't recognize that email and/or password."
