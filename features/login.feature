Feature: login

  Scenario: unauthorized & authorize users
    Given "authorize" user login
    Then should see username displayed on the screen
    When "unauthorized" user login
    Then should see an error message displayed on the screen